# Football Player Finder
 Este es un buscador que filtra en una tabla los jugadores que cumplen con los datos escritos en el formulario.
##  Como clonar
Copie la URL del proyecto y corra el siguiente comando en su terminal.
```bash
git clone "La URL del proyecto"
```
Después de clonar el proyecto corra el comando:
```bash
cd football-player-finder
```
Luego debe instalar las dependencias con:
```bash
npm install
```
Para que el proyecto funcione, debe crear un archivo en la raíz del proyecto con el nombre **.env.local** y pegar la siguiente linea de código.
```bash
REACT_APP_API_URL = 'https://football-players-b31f2.firebaseio.com/players.json?print=pretty'
```
## Ejecución
Para ejecutar el proyecto solo debe ejecutar el comando:
```bash
npm start
```