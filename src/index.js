import React from 'react'
import ReactDOM from 'react-dom'
import { App } from './app/App'
import * as serviceWorker from './serviceWorker'
import { StoreProvider } from './store/store'
import { GlobalStyle } from './styles/GlobalStyles'

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider>
      <GlobalStyle />
      <App />
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root')
)

serviceWorker.unregister()
