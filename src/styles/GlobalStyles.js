import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
    body{
        margin: 0;
        font-family: "Segoe UI";
    }
    h1,p{
        margin: 0;
    }
`
