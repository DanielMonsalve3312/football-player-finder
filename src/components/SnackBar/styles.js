import styled from 'styled-components'

export const SnackBarWrapper = styled.div`
  width: 95%;
  margin: 0 auto;
  position: fixed;
  bottom: 40px;
  display: flex;
  justify-content: center;
`

export const SnackBarStyle = styled.div`
  max-width: 450px;
  background-color: #1e2022;
  border-radius: 10px;
  box-shadow: 3px 3px 5px 0px rgba(0, 0, 0, 0.7);
  p {
    padding: 14px;
    color: #fff;
  }
`
