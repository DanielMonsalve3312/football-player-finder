import React, { useEffect } from 'react'
import { SnackBarStyle, SnackBarWrapper } from './styles'
import { SnackBarAction } from './SnackBarAction'
import { useDispatch } from 'react-redux'

export default function SnackBar ({ message }) {
  const dispatch = useDispatch()

  useEffect(() => {
    const timeout = setTimeout(() => {
      dispatch(SnackBarAction({ open: false }))
    }, 3000)
    return () => {
      clearTimeout(timeout)
    }
  }, [])

  return (
    <SnackBarWrapper>
      <SnackBarStyle>
        <p>{message}</p>
      </SnackBarStyle>
    </SnackBarWrapper>
  )
}
