import { createAction } from 'redux-actions'

export const SnackBarAction = createAction('SNACKBAR')
