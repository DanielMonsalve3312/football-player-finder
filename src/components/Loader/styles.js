import styled, { keyframes } from 'styled-components'

const spiner = keyframes`
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
`

export const LoadingWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #818080;
  opacity: .3;
`

export const Svg = styled.svg`
  width: 90px;
  height: 90px;
  animation: ${spiner} 1.5s linear infinite;
`
