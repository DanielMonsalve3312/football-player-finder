import React from 'react'
import { LoadingWrapper } from './styles'
import BallComponent from './BallComponent'

export default function Loader () {
  return (
    <LoadingWrapper>
      <BallComponent />
    </LoadingWrapper>
  )
}
