import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

export function usePlayersFilter () {
  const [players, setPlayers] = useState([])
  const playerInTheStore = useSelector((state) => state.data.players)
  const searchedPlayerData = useSelector((state) => state.searchedPlayerData)

  useEffect(() => {
    const newArrayOfPlayers = []

    playerInTheStore.forEach((player) => {
      let addToTheList = true

      Object.keys(searchedPlayerData).forEach((key) => {
        let dataIsTrue = player[key] === Number(searchedPlayerData[key])
        if (key !== 'age') {
          dataIsTrue = player[key]
            .toLowerCase()
            .includes(searchedPlayerData[key].toLowerCase())
        }
        if (!dataIsTrue) addToTheList = false
      })

      if (addToTheList) newArrayOfPlayers.push(player)
    })

    setPlayers(newArrayOfPlayers)
  }, [searchedPlayerData, playerInTheStore])

  return players
}
