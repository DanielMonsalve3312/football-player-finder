import React from 'react'
import { TableRow } from './styles'

export function TableRowPlayers ({ player }) {
  return (
    <TableRow>
      <td>{player.name}</td>
      <td>{player.position}</td>
      <td>{player.nationality}</td>
      <td>{player.age}</td>
    </TableRow>
  )
}
