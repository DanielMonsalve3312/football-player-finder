import React from 'react'
import { Table, TableRow, Paragraph } from './styles'
import { TableRowPlayers } from './TableRowPlayers'
import { usePlayersFilter } from './usePlayersFilter'

export default function TableContainer () {
  const players = usePlayersFilter()
  return (
    <div>
      <Table>
        <thead>
          <TableRow bgColor>
            <td>Player</td>
            <td>Position</td>
            <td>Nationality</td>
            <td>Age</td>
          </TableRow>
        </thead>
        <tbody>
          {players.map((player, id) => (
            <TableRowPlayers player={player} key={id} />
          ))}
        </tbody>
      </Table>
      {players.length === 0 && (
        <Paragraph>No results found =(</Paragraph>
      )}
    </div>
  )
}
