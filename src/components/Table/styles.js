import styled from 'styled-components'

export const Table = styled.table`
  width: 100%;
  border: 4px solid #fff;
  border-collapse: collapse;
`

export const TableRow = styled.tr`
  background-color: ${(props) => props.bgColor ? '#818282' : 'transparent'};
  color: ${(props) => props.bgColor ? '#fff' : 'initial'};
  & td {
    font-weight: 600;
    border: 4px solid #fff;
    text-align: center;
    padding: 15px 5px;
  }
`

export const Paragraph = styled.p`
  margin-top: 50px;
`
