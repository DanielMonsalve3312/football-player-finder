import styled from 'styled-components'

export const ErrorContainer = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`

export const ImageContainer = styled.div`
    width: 300px;
`
export const Image = styled.img`
    width: 100%;
`
export const ErrorMessage = styled.h1`
    color: red;
    font-weight: 500;
`
