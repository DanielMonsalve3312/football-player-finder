import React from 'react'
import error from '../../images/error.png'
import { ErrorContainer, ImageContainer, Image, ErrorMessage } from './styles'

export default function FailedToFetch ({ message }) {
  return (
    <ErrorContainer>
      <ImageContainer>
        <Image src={error} alt='error' />
      </ImageContainer>
      <ErrorMessage>{message}</ErrorMessage>
    </ErrorContainer>
  )
}
