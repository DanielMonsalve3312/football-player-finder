import { SnackBarAction } from '../SnackBar/SnackBarAction'

const callToSnackBar = (message, dispatch) => {
  dispatch(
    SnackBarAction({
      open: true,
      message
    })
  )
}

export const validateName = (name, dispatch, setName) => {
  const regex = /\d/g
  const result = regex.test(name)
  if (result) {
    callToSnackBar('Cannot put numbers in the Playername field', dispatch)
  } else {
    setName({ name })
  }
}

export const validateAge = (age, dispatch, setAge, buttonRef) => {
  const number = Number(age)
  if (number || age === '') {
    if (age !== '' && (number < 18 || number > 40)) {
      buttonRef.disabled = true
    } else {
      buttonRef.disabled = false
    }
    setAge({ age })
  } else {
    callToSnackBar('Letters cannot be placed in the Age field', dispatch)
  }
}

export const validateAgeRange = (age, dispatch, inputRef) => {
  if ((age < 18 || age > 40) && age !== '') {
    callToSnackBar(`The ${"player's"} age must be between 18 and 40 years`, dispatch)
    inputRef.focus()
  }
}
