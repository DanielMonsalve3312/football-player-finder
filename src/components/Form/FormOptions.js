import React from 'react'
import { createSelector } from 'reselect'
import { useSelector } from 'react-redux'

const selectPositionOptions = state => state.positionOptions
const selectPlayerPositions = state => state.data.players

const selectPositions = createSelector(
  selectPositionOptions,
  selectPlayerPositions,
  (options, players) => {
    players.forEach(player => {
      const boolean = options.includes(player.position)
      if (!boolean) {
        options.push(player.position)
      }
    })
    return options
  }
)

export function FormOptions () {
  const options = useSelector(selectPositions)
  return (
    <>
      <option value=''>Select position</option>
      {options.map((option, id) => (
        <option key={id} value={option}>{option}</option>
      ))}
    </>
  )
}
