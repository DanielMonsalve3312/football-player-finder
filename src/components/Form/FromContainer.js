import React, { useState, useRef } from 'react'
import { TextField, Select, Form, ButtonGroup, Button, FormWrapper } from './styles'
import { useDispatch } from 'react-redux'
import { validateName, validateAge, validateAgeRange } from './validateTextFields'
import { FormOptions } from './FormOptions'
import { FromAction } from './FormAction'

export default function FromContainer () {
  const [clear, setClear] = useState(false)
  const [state, setState] = useState({
    name: '',
    position: '',
    age: ''
  })
  const inputAge = useRef(null)
  const buttonSubmit = useRef(null)
  const dispatch = useDispatch()

  const changeStatus = (value) => {
    setState({
      ...state,
      ...value
    })
  }

  const handleNameChange = (e) => {
    validateName(e.target.value, dispatch, changeStatus)
  }

  const handleSelectChange = e => {
    changeStatus({
      position: e.target.value
    })
  }

  const handleAgeChange = e => {
    validateAge(e.target.value, dispatch, changeStatus, buttonSubmit.current)
  }

  const handleAgeBlur = () => {
    validateAgeRange(state.age, dispatch, inputAge.current, buttonSubmit.current)
  }

  const handleSearch = () => {
    const searchData = {}
    let data = false
    Object.keys(state).forEach(key => {
      if (state[key] !== '') {
        searchData[key] = state[key]
        data = true
      }
    })
    if (data) {
      setClear(true)
      dispatch(FromAction(searchData))
    }
  }

  const handleClear = () => {
    setClear(false)
    changeStatus({
      name: '',
      position: '',
      age: ''
    })
    dispatch(FromAction({
      name: ''
    }))
  }

  return (
    <FormWrapper>
      <Form>
        <TextField
          type='text'
          placeholder='Playername'
          name='name'
          value={state.name}
          onChange={handleNameChange}
        />
        <Select onChange={handleSelectChange} value={state.position}>
          <FormOptions />
        </Select>
        <TextField
          type='text'
          placeholder='Age'
          name='age'
          value={state.age}
          onChange={handleAgeChange}
          ref={inputAge}
          onBlur={handleAgeBlur}
        />
      </Form>
      <ButtonGroup>
        <Button color='primary' onClick={handleSearch} ref={buttonSubmit}>Search</Button>
        {clear && <Button color='secondary' onClick={handleClear} border>Clear</Button>}
      </ButtonGroup>
    </FormWrapper>
  )
}
