import styled, { css } from 'styled-components'
import arrow from '../../images/arrow.png'

const colors = {
  primary: '#ea0027',
  secondary: '#c9c9c9'
}

const formFields = () => {
  return css`
    width: 90%;
    font-size: 16px;
    padding: 10px;
    border: 2px solid ${colors.primary};
    border-radius: 20px;
    outline: none;
    margin-left: 15px;
    @media (max-width: 640px) {
      margin-bottom: 10px;
      width: auto;
      margin-left: 0;
    }
  `
}

export const FormWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-items: flex-start;
  margin-bottom: 40px;
  @media (max-width: 767px) {
    flex-direction: column;
  }
`

export const Form = styled.form`
  width: 100%;
  display: flex;
  justify-items: center;
  @media (max-width: 640px) {
    flex-direction: column;
  }
`

export const TextField = styled.input`
  ${formFields()}
`

export const Select = styled.select`
  ${formFields()}
  cursor: pointer;
  appearance: none;
  background-image: url(${arrow});
  background-repeat: no-repeat;
  background-position: 95%;
  background-size: 15px;
`

export const ButtonGroup = styled.div`
  width: 250px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  @media (max-width: 767px) {
    flex-direction: initial;
    width: 100%;
    margin-top: 10px;
  }
  @media (max-width: 640px) {
    flex-direction: column;
    width: 100%;
  }
`

export const Button = styled.button`
  width: 80%;
  background-color: ${(props) => colors[props.color]};
  color: ${(props) => (props.border ? colors.primary : '#fff')};
  border: ${(props) => (props.border ? `3px solid ${colors.primary}` : 'none')};
  border-radius: 20px;
  padding: 8px;
  font-size: 16px;
  margin-bottom: 10px;
  outline: none;
  transition: transform 500ms;
  cursor: pointer;
  &:active{
    transform: scale(.5);
  }
  &[disabled] {
    background-color: #abb2b9;
  }
  @media (max-width: 767px) {
    width: 40%;
  }
  @media (max-width: 640px) {
    width: 100%;
  }
`
