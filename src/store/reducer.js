export const reducer = (state, action) => {
  switch (action.type) {
    case 'GET_PLAYERS':
      return {
        ...state,
        data: {
          ...state.data,
          ...action.payload
        }
      }
    case 'SEARCH': {
      return {
        ...state,
        searchedPlayerData: action.payload
      }
    }
    case 'SNACKBAR':
      return {
        ...state,
        snackBar: action.payload
      }
    default:
      return state
  }
}
