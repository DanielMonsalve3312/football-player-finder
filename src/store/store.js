import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import { reducer } from './reducer'
import thunk from 'redux-thunk'

const initialState = {
  data: {
    players: [],
    loading: true,
    error: null
  },
  searchedPlayerData: {
    name: ''
  },
  positionOptions: [],
  snackBar: {
    open: false,
    message: ''
  }
}

export const store = createStore(reducer, initialState, applyMiddleware(thunk))

export const StoreProvider = ({ children }) => (
  <Provider store={store}>{children}</Provider>
)
