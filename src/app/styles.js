import styled from 'styled-components'
import image from '../images/ball.svg'

export const Wrapper = styled.div`
    height: 100%;
    min-height: 100vh;
    background-image: url(${image});
    background-size: 50vw;
    background-position: 70vw 40vh;
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-color:#D2D2D2;
    @media (max-width: 640px){
    background-size: 80vw;
    background-position: 70vw 45vh;
  }
`

export const Container = styled.div`
  max-width: 1200px;
  text-align: center;
  margin: 0 auto;
  padding-bottom: 50px;
  @media (max-width: 1200px) {
    width: 95%;
  }
  @media (max-width: 381px){
    width: 100%;
  }
`

export const Title = styled.h1`
  font-weight: 700;
  padding: 40px 0px;
  color: #505050
`
