import { createAction } from 'redux-actions'

const baseURl = process.env.REACT_APP_API_URL
const currentYear = new Date().getFullYear()
const currentMouth = new Date().getMonth() + 1
const currentDay = new Date().getDate()

const GetPlayersAction = createAction('GET_PLAYERS', (object) => {
  return {
    ...object,
    loading: false
  }
})

const calculateAge = (players, dispatch) => {
  players.forEach((player) => {
    const dateOfBirth = player.dateOfBirth.split('-')
    let age = currentYear - dateOfBirth[0]
    if (dateOfBirth[1] > currentMouth) age = age - 1
    else if (Number(dateOfBirth[1]) === currentMouth) {
      if (currentDay < Number(dateOfBirth[2])) age = age - 1
    }
    player.age = age
  })
  dispatch(GetPlayersAction({ players }))
}

export const GetPlayers = () => async (dispatch) => {
  try {
    const response = await window.fetch(baseURl)
    const players = await response.json()

    calculateAge(players, dispatch)
  } catch (error) {
    dispatch(GetPlayersAction({ error }))
  }
}
