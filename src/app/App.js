import React, { useEffect } from 'react'
import { Container, Title, Wrapper } from './styles'
import {
  SnackBar,
  Loader,
  TableContainer,
  FromContainer,
  FailedToFetch
} from '../components'
import { GetPlayers } from './GetPlayersAction'
import { useSelector, useDispatch } from 'react-redux'

export function App () {
  const { data, snackBar } = useSelector((state) => state)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(GetPlayers())
  }, [])

  if (data.loading) return <Loader />

  if (data.error) return <FailedToFetch message={data.error.message} />

  return (
    <Wrapper>
      <Container>
        <Title>Football player finder</Title>
        <FromContainer />
        <TableContainer />
        {snackBar.open ? <SnackBar message={snackBar.message} /> : null}
      </Container>
    </Wrapper>
  )
}
